package lp1_ficha5;

/**
 * @authors Bruno e Daniel
 *
 */

public class lp1_ficha5 {
    public static void main(String[] args) {
        int counter = 0;
        String numeragem = "";

        for (int i = 1 ; i < 11 ; i++) {
            for (int l = 0 ; l < 11 ; l++) {
                counter++;

                if (counter % 3 == 0 && counter % 5 == 0) {
                    System.out.print(numeragem);
                    System.out.print(" FEIRAISEP ");

                } else if (counter % 3 == 0 && counter % 7 == 0) {
                    System.out.print(numeragem);
                    System.out.print(" FEIRAIPP ");

                } else if (counter % 3 == 0) {
                    System.out.print(numeragem);
                    System.out.print(" FEIRA ");

                } else if (counter % 5 == 0) {
                    System.out.print(numeragem);
                    System.out.print(" ISEP ");

                } else if (counter % 7 == 0) {
                    System.out.print(numeragem);
                    System.out.print(" IPP ");

                } else {
                    System.out.print (numeragem);
                    System.out.print (" " + counter + " ");
                }
            }
            System.out.println ();
        }
    }
}
